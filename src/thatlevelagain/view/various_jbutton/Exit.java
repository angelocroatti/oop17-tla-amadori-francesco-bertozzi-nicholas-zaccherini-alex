package thatlevelagain.view.various_jbutton;


/**
 * 
 * exit button in menu.
 *
 */
public class Exit extends ButtonGeneral {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final String NAME = "EXIT GAME";
    /**
     * constructor.
     */
    public Exit() {
        super(NAME);
    }

}
