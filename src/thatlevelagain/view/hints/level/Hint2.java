package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level2's hint.
 *
 */
public class Hint2 extends HintImpl {

    private static final String MESSAGE = "PRESS THE BUTTON, IF YOU CAN...";
    private static final String NAME = "JUMP FROM THE LEFT PLATFORM OF THE BUTTON ON IT, PAYING ATTENTION TO THE THORNS";

    /**
     * 
     */
    public Hint2() {
        super(NAME, MESSAGE);
    }

}
