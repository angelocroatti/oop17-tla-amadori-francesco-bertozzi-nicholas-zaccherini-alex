package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level10's hint.
 *
 */
public class Hint10 extends HintImpl {

    private static final String MESSAGE = "KNOCK KNOCK..";
    private static final String NAME = "TOUCH THE DOOR THREE TIMES";

    /**
     * 
     */
    public Hint10() {
        super(NAME, MESSAGE);
    }

}

